package qaHelp;

import java.util.HashMap;
import java.util.logging.Logger;

public class BannersAndCarouselItemsTestData {
    public static HashMap<String, String> bannersMap = new HashMap<>();
    public static HashMap<String, String> carouselItemsMap = new HashMap<>();
    private static Logger logger = Logger.getLogger(BannersAndCarouselItemsTestData.class.getName());

    static {
        bannersMap.put("hybris Accelerator", "Apparel Site UK | Homepage");
        bannersMap.put("Start Your Season", "Snow");
        bannersMap.put("Save Big on Select Streetwear", "Streetwear");
        bannersMap.put("Women", "women");
        bannersMap.put("Men", "men");
        bannersMap.put("Youth", "Streetwear youth");
        bannersMap.put("Our brand range", "Brands");
        bannersMap.put("Maguro Pu Belt plaid", "Maguro Pu Belt");
        bannersMap.put("Airline Bag Bluebird Uni", "Airline Bag");
        bannersMap.put("Adapt New Era Blacktop", "Adapt New Era");
        bannersMap.put("Shades Von Zipper Papa G", "Shades Von Zipper Papa G black gloss black gloss/grey");
        bannersMap.put("Free Shipping on All Orders This Weekend", "Frequently Asked Questions");
    }
    static {
        carouselItemsMap.put("Snowboard Ski Tool Toko Ergo Multi Guide yellow", "SNOWBOARD SKI TOOL TOKO ERGO MULTI GUIDE YELLOW");
        carouselItemsMap.put("Snowboard Ski Tool Toko Waxremover HC3 500ml" , "Snowboard Ski Tool Toko Waxremover HC3 500ml");
        carouselItemsMap.put("Helmet Women TSG Lotus Graphic Designs wms", "HELMET WOMEN TSG LOTUS GRAPHIC DESIGNS WMS");
        carouselItemsMap.put("Wallet Dakine Agent Leather Wallet brown", "WALLET DAKINE AGENT LEATHER WALLET BROWN");
        carouselItemsMap.put("Shades Quiksilver Dinero black white red grey", "Shades Quiksilver Dinero black white red grey");
        carouselItemsMap.put("Snowboard Ski Tool Toko Ergo Speed Top 88°/89°", "Snowboard Ski Tool Toko Ergo Speed Top 88°/89°");
        carouselItemsMap.put("Shades Von Zipper Papa G white grey chrome", "Shades Von Zipper Papa G white grey chrome");
        carouselItemsMap.put("Snowboard Ski Tool Toko Side Edge Angle Pro 88 Grad", "Snowboard Ski Tool Toko Side Edge Angle Pro 88 Grad");
        carouselItemsMap.put("Shades Fox The Duncan polished black grey", "Shades Fox The Duncan polished black grey");
        carouselItemsMap.put("Shades Von Zipper Fernstein gold moss gradient", "Shades Von Zipper Fernstein gold moss gradient");
        carouselItemsMap.put("Shades Women Roxy Carla roxy black grey", "SHADES WOMEN ROXY CARLA ROXY BLACK GREY");
        carouselItemsMap.put("Protector Dainese Waistcoat S7", "Protector Dainese Waistcoat S7");
        carouselItemsMap.put("Helmet Women TSG Lotus Graphic Designs wms", "Helmet Women TSG Lotus Graphic Designs wms");
        carouselItemsMap.put("Snowboard Ski Tool Toko T8 800 Wachsbügeleisen für Heißwachs", "SNOWBOARD SKI TOOL TOKO T8 800 WACHSBÜGELEISEN FÜR HEISSWACHS");
        carouselItemsMap.put("Snowglasses adidas eyewear Yodai black matt orange mirror", "Snowglasses adidas eyewear Yodai black matt orange mirror");
        carouselItemsMap.put("Cap Blue Tomato BT Snow Trucker Cap black", "Cap Blue Tomato BT Snow Trucker Cap black");
    }
}
