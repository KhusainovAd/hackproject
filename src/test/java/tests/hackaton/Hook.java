package tests.hackaton;

import io.cucumber.java.After;
import io.cucumber.java.Before;

import java.util.logging.Logger;

public class Hook extends DriverFactory {
    private static Logger logger = Logger.getLogger(Hook.class.getName());


    @Before
    public void initDriver() {
        //подготовить данные
/*        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("test-type");
        options.addArguments("--start-maximized");
        options.addArguments("--disable-web-security");
        options.addArguments("--allow-running-insecure-content");
        options.addArguments("--ignore-ssl-errors=yes");
        options.addArguments("--ignore-certificate-errors");
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        logger.info("Инициализируем браузер");
        driver = new ChromeDriver(capabilities);*/
    }

    @After
    public void tearDown() {
        logger.info("Закрываем браузер");
        destroyDriver();
    }
}